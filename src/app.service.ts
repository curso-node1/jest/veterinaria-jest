import RegisterDogDto from "./dto/register-dog.dto";
import { getModelForClass } from "@typegoose/typegoose";
import { VeterinaryModel } from './models/veterinary.model';
import { DogModel } from "./models/dog.model";

class AppService {
  /**
   * Método que hace el registro del perrito en una veterinaria
   */
  async registerDogInVeterinary(registerDogDto: RegisterDogDto) {
    const veterinaryModel = getModelForClass(VeterinaryModel);
    const dogModel = getModelForClass(DogModel);

    const vets = await veterinaryModel.find();
    console.log('Veterinarias: ', vets);
  }
}

export default new AppService();

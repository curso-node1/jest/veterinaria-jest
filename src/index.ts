require('dotenv').config();
import { ResponseMicroservice } from './dto/response-microservice.dto';
import { MongoConnection } from './database/MongoConnection';
import { RequestMicroservice } from './dto/request-microservice.dto';
import { validateRequest } from './utils/validator';
import appService from './app.service';
class MicroserviceExample {
  mongoConnection: MongoConnection;
  constructor() {
    this.mongoConnection = new MongoConnection();
    this.mongoConnection.connectToMongo();
  }

  /**
   * Este recibe los datos del perrito y se procede a hacer el registro
   */
  public async global(plainRequestMicroservice: RequestMicroservice): Promise<ResponseMicroservice> {
    try {
      // Aqui se valida la petición.
      await validateRequest(RequestMicroservice, plainRequestMicroservice);

      await appService.registerDogInVeterinary({
        dog: {
          name: plainRequestMicroservice.nameDog,
          color: plainRequestMicroservice.colorDog,
        },
        veterinaryId: plainRequestMicroservice.veterinaryId
      });
      return { todoBien: true };
    } catch (error) {
      console.log('Hubo un error:', error);
      // throw this.emitError(-22, error);
    }
  }
}

// Aqui simulamos que se esta llamando al microservicio
let microservice = new MicroserviceExample();
let dataRequest = {
  nameDog: 'Husky',
  colorDog: 'Rojo',
  age: 1,
  veterinaryId: '5e94af2aa9deae0db9d60b0d',
};
microservice.global(dataRequest);
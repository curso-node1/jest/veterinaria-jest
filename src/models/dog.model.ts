import { prop, modelOptions, Ref } from '@typegoose/typegoose';
import { VeterinaryModel } from './veterinary.model';
import { IsNotEmpty, IsIn, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

@modelOptions({ schemaOptions: { collection: 'dogs' } })
export class DogModel {
  /** Id de mongo */
  _id: string;

  /** Este es el nombre del perrito */
  @IsNotEmpty()
  @prop()
  name: string;

  /** Este es el color del perrito */
  @IsNotEmpty()
  @prop()
  color: string;
  
  @Type(() => VeterinaryModel)
  @ValidateNested()
  @prop()
  veterinary: VeterinaryModel;
}

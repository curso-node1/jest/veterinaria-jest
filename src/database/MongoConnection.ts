import { connect, connection, Connection } from 'mongoose';

export class MongoConnection {
  private mongoURL: any = process.env.MONGO_URL || '';
  public db: Connection;

  /**
   * Función que hace la conexión de mongo
   */
  public async connectToMongo () {
    const MONGO_CONNECTION_OPTIONS = {
      useNewUrlParser: true,
      autoIndex: false,
      poolSize: 10,
      connectTimeoutMS: 10000,
      useUnifiedTopology: true
    };
    connect(this.mongoURL, MONGO_CONNECTION_OPTIONS);
    this.db = connection;
    this.db.on('error', console.error.bind(console, 'Error al conectar a MongoDB:'));
    this.db.on('connected', () => {
      console.log('Conectado correctamente a MongoDB.');
    });
    /* this.db.once('open', () => {
      console.log('Conectado correctamente a MongoDB.');
    }); */
    return true;
  };
}

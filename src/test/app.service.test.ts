import { MongoConnectionTest } from "./database/MongoConnectionTest";
import appServiceTestSetup from './setup';
import appService from "../app.service";
import { DOG_TEST_CASE, VETERINARY_ID_THAT_NOT_EXIST, VETERINARY_ID_TEST_CASE, VETERINARY_TEST_CASE } from "./constants";
import { getModelForClass } from "@typegoose/typegoose";
import { DogModel } from "../models/dog.model";
import { VeterinaryModel } from '../models/veterinary.model';

describe('app.service.ts', () => {
  let mongoConnectionTest: MongoConnectionTest;
  let dogModel = getModelForClass(DogModel);
  let veterinaryModel = getModelForClass(VeterinaryModel);
  beforeAll(async () => {
    mongoConnectionTest = new MongoConnectionTest();
    await mongoConnectionTest.connectToMongo();
  });
  beforeEach(async () => {
    await appServiceTestSetup.setUpVeterinary();
  });
  afterEach(async () => {
    await mongoConnectionTest.removeCollectionsFromTemporaryStore();
  });

  describe('test function registerDogInVeterinary', () => {
    test('Debe lanzar una excepción si la veterinaria no se encontró en la base de datos', async () => {

    });
    test('Debe guardar y registrar el perrito si se encontró la veterinaria', async () => {

    });
    test('Asignar la nueva veterinaria si el perrito ya esta guardado en la base de datos', () => {

    });
  });
});
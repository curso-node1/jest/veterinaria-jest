import { getModelForClass } from "@typegoose/typegoose";
import { VeterinaryModel } from "../models/veterinary.model";
import { VETERINARY_TEST_CASE } from "./constants";

/**
 * Guarda una guardería con un id para poder hacer pruebas
 */
const setUpVeterinary = async ()  => {
  const veterinaryModel = getModelForClass(VeterinaryModel);
  await veterinaryModel.create(VETERINARY_TEST_CASE);
}

export default {
  setUpVeterinary
}
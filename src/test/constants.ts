import { Types } from "mongoose";

export const VETERINARY_TEST_CASE = {
  _id: Types.ObjectId('56cb91bdc3464f14678934ca'),
  name: 'Veterinaria de prueba',
  address: 'Esta en el mero Tigre'
}

export const VETERINARY_ID_TEST_CASE = '56cb91bdc3464f14678934ca';

export const DOG_TEST_CASE = {
  name: 'Oso',
  color: 'Rojo',
}

export const VETERINARY_ID_THAT_NOT_EXIST = '56cb91bdc3464f14678934cb';
